package com.fedorov.andrii.testapp.ui.pages.second

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fedorov.andrii.testapp.R
import com.fedorov.andrii.testapp.application.AppInstance
import com.fedorov.andrii.testapp.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity () {

    private val viewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val page = intent.getIntExtra(ARG_PAGE, -1).takeIf { it > 0 } ?: throw IllegalArgumentException()
                val todoRepository = (application as AppInstance).dependencyContainer.getTodoRepository()
                return SecondViewModel(page, todoRepository) as T
            }
        }).get(SecondViewModel::class.java)
    }

    companion object {
        private val ARG_PAGE = "ARG_PAGE"

        fun getStartIntent(context: Context, page: Int) = Intent(context, SecondActivity::class.java).apply {
            putExtra(ARG_PAGE, page)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivitySecondBinding>(this, R.layout.activity_second).apply {
            lifecycleOwner = this@SecondActivity
            viewModel = this@SecondActivity.viewModel
        }
    }

}