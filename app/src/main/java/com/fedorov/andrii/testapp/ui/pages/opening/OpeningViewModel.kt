package com.fedorov.andrii.testapp.ui.pages.opening

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fedorov.andrii.testapp.kit.repositories.PageCounterRepository
import com.fedorov.andrii.testapp.ui.utils.ActionLiveData
import kotlinx.coroutines.launch

class OpeningViewModel(private val pageCounterRepository: PageCounterRepository): ViewModel() {

    val onOpenNextPage = ActionLiveData<Int>()

    fun openNextPage() {
        viewModelScope.launch {
            val nextOpenedPageCount =  pageCounterRepository.getOpenedPageCount() + 1
            pageCounterRepository.setOpenedPageCount(nextOpenedPageCount)
            onOpenNextPage.value = nextOpenedPageCount
        }
    }
}