package com.fedorov.andrii.testapp.kit.mapper

import com.fedorov.andrii.testapp.core.Todo
import com.fedorov.andrii.testapp.network.entity.TodoNT
import com.fedorov.andrii.testapp.storage.entity.TodoST

fun TodoNT.mapToCore() = Todo(
    id,
    userId,
    title,
    completed
)

fun TodoST.mapToCore() = Todo(
    id,
    userId,
    title,
    completed
)

fun List<TodoST>.mapToCore() = map { it.mapToCore() }

fun Todo.mapToStorage() = TodoST(
    id,
    userId,
    title,
    completed
)