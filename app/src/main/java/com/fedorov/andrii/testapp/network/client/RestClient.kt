package com.fedorov.andrii.testapp.network.client

import com.fedorov.andrii.testapp.network.services.TodoApiService
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RestClient {

    private val restClient = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com")
        .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder().build()))
        .build()

    fun getTodoApiService() = restClient.create(TodoApiService::class.java)
}