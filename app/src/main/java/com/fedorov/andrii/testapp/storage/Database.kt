package com.fedorov.andrii.testapp.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fedorov.andrii.testapp.storage.dao.PageCounterDao
import com.fedorov.andrii.testapp.storage.dao.TodoDao
import com.fedorov.andrii.testapp.storage.entity.TodoST
import com.fedorov.andrii.testapp.storage.prefs.PageCounterPreferences

@Database(entities = [TodoST::class], version = 1)
abstract class Database: RoomDatabase() {

    private val pageCounterPreferences = PageCounterPreferences("PageCounterPreferences")

    fun getPageCounterDao() = PageCounterDao(pageCounterPreferences)

    abstract fun getTodoDao() : TodoDao
}