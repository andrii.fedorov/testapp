package com.fedorov.andrii.testapp.application.di

import com.fedorov.andrii.testapp.kit.repositories.PageCounterRepository
import com.fedorov.andrii.testapp.kit.repositories.TodoRepository
import com.fedorov.andrii.testapp.network.client.RestClient
import com.fedorov.andrii.testapp.storage.Database

class DependencyContainer(private val database: Database, private val restClient: RestClient) {

    fun getPageCounterRepository() = PageCounterRepository(database.getPageCounterDao())

    fun getTodoRepository() = TodoRepository(restClient.getTodoApiService(), database.getTodoDao())
}