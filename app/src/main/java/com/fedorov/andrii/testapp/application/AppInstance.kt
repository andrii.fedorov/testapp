package com.fedorov.andrii.testapp.application

import android.app.Application
import androidx.room.Room
import com.chibatching.kotpref.Kotpref
import com.fedorov.andrii.testapp.application.di.DependencyContainer
import com.fedorov.andrii.testapp.network.client.RestClient
import com.fedorov.andrii.testapp.storage.Database

class AppInstance : Application() {

    val dependencyContainer by lazy {
        DependencyContainer(
            Room.databaseBuilder(this, Database::class.java, "TestAppDatabase").build(),
            RestClient()
        )
    }

    override fun onCreate() {
        super.onCreate()
        Kotpref.init(applicationContext)

    }
}