package com.fedorov.andrii.testapp.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.fedorov.andrii.testapp.storage.entity.TodoST

@Dao
interface TodoDao {

    @Insert
    suspend fun insert(vararg todos: TodoST)

    @Query("SELECT * FROM todo")
    suspend fun getTodos(): List<TodoST>

}