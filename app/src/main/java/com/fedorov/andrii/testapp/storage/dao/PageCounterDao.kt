package com.fedorov.andrii.testapp.storage.dao

import com.fedorov.andrii.testapp.storage.prefs.PageCounterPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PageCounterDao(private val pageCounter: PageCounterPreferences) {

    suspend fun setOpenedPageCount(openedPageCount: Int) = withContext(Dispatchers.IO) {
        pageCounter.openedPageCount = openedPageCount
    }

    suspend fun getOpenedPageCount() = withContext(Dispatchers.IO) {
        pageCounter.openedPageCount
    }
}