package com.fedorov.andrii.testapp.kit.repositories

import com.fedorov.andrii.testapp.storage.dao.PageCounterDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PageCounterRepository(private val pageCounterDao: PageCounterDao) {

    suspend fun setOpenedPageCount(openedPageCount: Int) = withContext(Dispatchers.IO) {
        pageCounterDao.setOpenedPageCount(openedPageCount)
    }

    suspend fun getOpenedPageCount() = withContext(Dispatchers.IO) {
        pageCounterDao.getOpenedPageCount()
    }
}