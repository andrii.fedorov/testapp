package com.fedorov.andrii.testapp.kit.repositories

import com.fedorov.andrii.testapp.core.Todo
import com.fedorov.andrii.testapp.kit.mapper.mapToCore
import com.fedorov.andrii.testapp.kit.mapper.mapToStorage
import com.fedorov.andrii.testapp.network.services.TodoApiService
import com.fedorov.andrii.testapp.storage.dao.TodoDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TodoRepository(
    private val todoApiService: TodoApiService,
    private val todoDao: TodoDao
) {

    suspend fun getTodoById(id: Int): Todo = withContext(Dispatchers.IO) {
        val todo = todoApiService.getTodoById(id).mapToCore()
        todoDao.insert(todo.mapToStorage())
        todo
    }

    suspend fun getCashedTodos() = withContext(Dispatchers.IO) {
        todoDao.getTodos().mapToCore()
    }
}