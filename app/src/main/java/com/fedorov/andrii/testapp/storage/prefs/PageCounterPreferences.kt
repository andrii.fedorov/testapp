package com.fedorov.andrii.testapp.storage.prefs

import com.chibatching.kotpref.KotprefModel

class PageCounterPreferences(name: String) : KotprefModel() {

    override val kotprefName = name

    var openedPageCount by intPref(0)

}