package com.fedorov.andrii.testapp.network.services

import com.fedorov.andrii.testapp.network.entity.TodoNT
import retrofit2.http.GET
import retrofit2.http.Path

interface TodoApiService {

    @GET("/todos/{id}")
    suspend fun getTodoById(@Path("id") todoId: Int): TodoNT
}