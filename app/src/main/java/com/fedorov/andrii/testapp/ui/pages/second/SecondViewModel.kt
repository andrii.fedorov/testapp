package com.fedorov.andrii.testapp.ui.pages.second

import androidx.lifecycle.*
import com.fedorov.andrii.testapp.core.Todo
import com.fedorov.andrii.testapp.kit.repositories.TodoRepository
import kotlinx.coroutines.launch

class SecondViewModel(
    private val openedCountPage: Int,
    private val todoRepository: TodoRepository
) : ViewModel() {

    val openedPageCount = MutableLiveData<Int>()
    val userId = MutableLiveData<Int>()
    val title = MutableLiveData<String>()
    val image = MutableLiveData<String>()

    val isProgressVisible = MutableLiveData<Boolean>()
    val isCantLoadDataVisible = MutableLiveData<Boolean>()
    val isContentVisible by lazy {
        MutableLiveData<Boolean>().also {
            loadTodo()
        }
    }

        private val starImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Five-pointed_star.svg/1200px-Five-pointed_star.svg.png"
        private val sunImageUrl = "https://solarsystem.nasa.gov/system/basic_html_elements/11561_Sun.png"

        private fun loadTodo() {
            viewModelScope.launch {
                isProgressVisible.value = true
                isContentVisible.value = false

                var result = Result.runCatching { todoRepository.getTodoById(openedCountPage) }
                if (result.isFailure) {
                    result = Result.runCatching {
                        todoRepository.getCashedTodos().sortedBy { it.id }
                            .reversed()
                            .first()
                    }
                }
                result.onSuccess {
                    openedPageCount.value = openedCountPage
                    userId.value = it.userId
                    title.value = it.title
                    image.value = if (it.completed) starImageUrl else sunImageUrl
                    isContentVisible.value = true
                }
                    .onFailure {
                        isCantLoadDataVisible.value = true
                    }
                isProgressVisible.value = false
            }
        }
}