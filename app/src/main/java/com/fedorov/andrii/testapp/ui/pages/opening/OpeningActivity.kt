package com.fedorov.andrii.testapp.ui.pages.opening

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fedorov.andrii.testapp.R
import com.fedorov.andrii.testapp.application.AppInstance
import com.fedorov.andrii.testapp.databinding.ActivityOpeningBinding
import com.fedorov.andrii.testapp.ui.pages.second.SecondActivity

class OpeningActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return OpeningViewModel((application as AppInstance).dependencyContainer.getPageCounterRepository()) as T
            }
        }).get(OpeningViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityOpeningBinding>(this, R.layout.activity_opening).apply {
            lifecycleOwner = this@OpeningActivity
            viewModel = this@OpeningActivity.viewModel
        }

        viewModel.apply {
            onOpenNextPage.observe(this@OpeningActivity) {
                startActivity(SecondActivity.getStartIntent(this@OpeningActivity, it))
            }
        }
    }

}